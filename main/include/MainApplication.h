/*
 * Panel.h
 *
 *  Created on: 30 ���. 2021 �.
 *      Author: bogd
 */

#ifndef MAIN_INCLUDE_MAINAPPLICATION_H_
#define MAIN_INCLUDE_MAINAPPLICATION_H_

#include <stdint.h>
#include <stdbool.h>
//#include "freertos/queue.h"

#define MESSAGE_LENGTH 32          //base message length, mainly depended by radio requirements
#define MAX_JSON_MESSAGE 256       //max size of mqtt message to publish

#define BAT_CHECK_INTERVAL  600
#define BAT_LOAD_PERIOD 3

#define MESSAGE_IMEI_OFFSET 1
#define MESSAGE_TECHDATA_OFFSET 5
#define MESSAGE_EVENTCODE_OFFSET 9
#define MESSAGE_EVENTDATA_OFFSET 13
#define MESSAGE_TIMESTAMP_OFFSET 21
#define MESSAGE_COUNTER_OFFSET 25
#define MESSAGE_CRC32_OFFSET 28

#define FAULT_DETECTION
#define ITERATION_NUM 10
#define ADC_ZONES_NUMBER 8



typedef struct
{
    uint16_t zonezAlarm[ADC_ZONES_NUMBER];
    uint16_t zonezFaultShort[ADC_ZONES_NUMBER];
    uint16_t zonezFaultOpen[ADC_ZONES_NUMBER];
    uint16_t zonezNorm[ADC_ZONES_NUMBER];

    uint16_t tamperAlarm;
    uint16_t tamperNorm;

} input_struct_t;

typedef enum
{
    SM_PAN_INIT = 0,
    SM_PAN_INPUT_CHECK
} panel_state_enum;

typedef enum
{
    EVENT_POWER_ON = 0,
    EVENT_PARTALARM_ALARM,
    EVENT_PARTALARM_RESTORE,
    EVENT_PARTFAULT_OPEN_ALARM,
    EVENT_PARTFAULT_OPEN_RESTORE,
    EVENT_PARTFAULT_SHORT_ALARM,
    EVENT_PARTFAULT_SHORT_RESTORE,
    EVENT_POWER_ALARM,
    EVENT_POWER_RESTORE,
    EVENT_AKKUM_DISCHARGE_ALARM,
    EVENT_AKKUM_DISCHARGE_RESTORE,
    EVENT_AKKUM_FAULT_ALARM,
    EVENT_AKKUM_FAULT_RESTORE,
    EVENT_TAMPER_ALARM,
    EVENT_TAMPER_RESTORE,
    EVENT_TEST
} EVENT_TYPE;

typedef enum
{
    BAT_NORM = 0,
    BAT_HALFDISCHARGED,
    BAT_DISCHSRGED,
    BAT_FAULT
} battery_state_enum;


typedef enum
{
    PUBLISH_PANEL_MESS_STRUCT = 0,
    PUBLISH_RAW_DATA,
} publish_data_type;

#pragma pack(push, 1)

typedef struct
{
    //technical extra data not included in transmission
    bool isActive;
    signed int RadioLevel;
    uint8_t DataCorrectionStatus;
    uint32_t PanelCounter;
    //data included in transmission
    union
    {
        uint8_t mess_bytes[MESSAGE_LENGTH];
        struct
        {
            uint8_t protocol_ver;
            uint8_t imei[4];
            uint32_t tech_data;
            uint32_t event_code;
            uint32_t event_data;
            uint8_t reserved[4];
            uint8_t timestamp[4];
            uint8_t counter[3];
            uint32_t crc32;
        } bytes;
        struct
        {
            uint8_t prot_ver :8;
            uint32_t imei :32;
            uint8_t :6;
            uint8_t tech_newevent :1;
            uint8_t tech_request :1;
            uint32_t :16;
            uint8_t tech_test1 :1;
            uint8_t tech_test2 :1;
            uint8_t :6;

            uint32_t event_code :32;
            uint8_t part_alarm :8;
            uint8_t part_fault :8;
            uint16_t evdata_unused :16;
            uint32_t reserved1 :32;
            uint32_t timestamp :32;
            uint32_t counter :24;
            uint32_t crc32 :32;
        } bits;
    };
} PANEL_MESS_STRUCT;
#pragma pack(pop)




void PanelInit(void);
void MainAppServiceStart(void);
void GetCounter(unsigned char *c);
void SendTestEvent(void);

#endif /* MAIN_INCLUDE_MAINAPPLICATION_H_ */
