/*
 * SoftwareConfiguration.h
 *
 *  Created on: 26 ����. 2021 �.
 *      Author: bogd
 */

#ifndef COMPONENTS_APPCORE_INCLUDE_SOFTWARECONFIGURATION_H_
#define COMPONENTS_APPCORE_INCLUDE_SOFTWARECONFIGURATION_H_
#ifdef __cplusplus
extern "C"
{
#endif
#include <stdint.h>
#include <stdbool.h>
#include "esp_netif.h"

#define MQTT_CLIENTS_NUM 2


#define DEFAULT_FLAG_PROTECT_ENABLED    false
#define DEFAULT_IMAX                    (500u)
#define DEFAULT_IMIN                    (0u)
#define DEFAULT_UMAX                    (260u)
#define DEFAULT_UMIN                    (0u)
#define DEFAULT_PROT_RESTORE_TIME       (60)
#define DEFAULT_PROT_RESTORE_ATTEMPTS   (5)

#define DEFAULT_PING_PERIOD             (60u)
#define DEFAULT_COUNT_LOST              (6u)
#define DEFAULT_PING_MAX_RESETS         (3u)
#define DEFAULT_PING_RESET_TIME         (10u)

#define DEFAULT_SUNEVENTS_ENABLED       false
#define DEFAULT_LON                     20.5f
#define DEFAULT_LAT                     54.7f
#define DEFAULT_TWILIGHT                (6u)
#define DEFAULT_ONRISE_ON               false
#define DEFAULT_ONSET_ON                true


#define DEFAULT_PING_GLOBAL_ENABLED     false
#define DEFAULT_PING_PERIOD             (60u)
#define DEFAULT_COUNT_LOST              (6u)
#define DEFAULT_PING_MAX_RESETS         (3u)
#define DEFAULT_PING_RESET_TIME         (10u)
#define DEFAULT_PINGURL1_ENABLED        true
#define DEFAULT_PINGURL2_ENABLED        true
#define DEFAULT_PINGURL1            "8.8.8.8"
#define DEFAULT_PINGURL2            "1.1.1.1"


//#define LOCK_RELAY_ON

// Application-dependent structure used to contain address information

    /**
     * @struct  APP_CONFIG
     * @brief The main configuration structure
     * @details This structure saving to EEPROM and loading from EEPROM\n
     * on device boot. On load the checksumm is calculate and compare to \n
     * saved one. If find difference (due to eeprom memory distortions),\n
     * the default values will be loaded.
     */
    typedef struct
    {
        struct
        {
            uint16_t Imax, Imin, Umax, Umin;
            uint16_t ProtRestoreTime;
            uint8_t ProtRestoreAttempts;
            struct
            {
                char bIsProtectEnable :1;
                char b1 :1;
                char b2 :1;
                char b3 :1;
                char b4 :1;
                char b5 :1;
                char b6 :1;
                char b7 :1;
            } Flags;
        } Protect;

        struct
        {
            struct
            {
                unsigned char GlobalPingEnabled :1;
                unsigned char URL1Enabled :1;
                unsigned char URL2Enabled :1;
                unsigned char NotUsed :5;
            } Flags; // Flag structure

            ip4_addr_t PingServerAdr1;
            ip4_addr_t PingServerAdr2;
            uint16_t PingPeriod;
            uint8_t CountLost;
            uint8_t MaxResets;
            uint8_t ResetTime;
        } Ping;

        struct
        {
            struct
            {
                unsigned char SunEventsEnabled :1;
                unsigned char OnRiseON :1;
                unsigned char OnSetON :1;
                unsigned char NotUsed :5;
            } Flags; // Flag structure

            double Lon;
            double Lat;
            uint8_t Twilight;
        } SunEvents;

        struct
        {
            struct
            {
                bool tarEnb;
                uint16_t tarBegin;
                uint16_t tarEnd;
                uint8_t tarZone;
            } TarifPool[6];

            uint8_t PriceUnit[4]; //Currency unit
            uint8_t Tarif1[7]; //Price 1
            uint8_t Tarif2[7]; //Price 2
            uint8_t Tarif3[7]; //Price 3
        } AppCounters;

    } APP_CONFIG;

#ifdef __cplusplus
}
#endif
#endif /* COMPONENTS_APPCORE_INCLUDE_SOFTWARECONFIGURATION_H_ */
