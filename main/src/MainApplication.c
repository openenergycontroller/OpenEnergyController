/*
 * Application.c
 *
 *  Created on: 06-05-2022
 *      Author: bogd
 */
#include "include/MainApplication.h"
#include <string.h>
#include "time.h"
#include "AppConfiguration.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "esp_timer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "Helpers.h"
#include "NetTransport.h"



ESP_EVENT_DEFINE_BASE(MAINAPP);
ESP_EVENT_DECLARE_BASE(MAINAPP);

typedef enum
{
    PANEL_EVENT_ANY = -1,
    PANEL_EVENT_ERROR = 0,
} esp_panel_event_id_t;

#define BAT_DEBUG

#define DEFAULT_VREF    1100        //Use adc2_vref_to_gpio() to obtain a better estimate

#define TIME_ZONE (+2)   //GMT+2
#define YEAR_BASE (1900) //tm structure base year

const unsigned TEST_INTERVAL = 480;
input_struct_t hInp;

static void MainAppServiceSecondary(void *arg);
void MainAppService(void *arg);
esp_timer_handle_t mainapp_service_timer;
const esp_timer_create_args_t mainapp_service_timer_args = {
        .callback = &MainAppService,
        .name = "MainAppTimer"
};

uint32_t UpTime = 0;

void MainAppService(void *pvParameter)
{
    static int FirmwareUpdateCounter = 0;
    ++UpTime;


#ifdef  DISPLAY_TYPE_SH1106
        time_t now;
        struct tm timeinfo;
        time(&now);
        localtime_r(&now, &timeinfo);
        char T[32];
        sprintf(T, "%04u/%02u/%02u   %02u:%02u:%02u",
                (timeinfo.tm_year) + YEAR_BASE,
                (timeinfo.tm_mon) + 1,
                timeinfo.tm_mday,
                timeinfo.tm_hour + TIME_ZONE, timeinfo.tm_min,
                timeinfo.tm_sec);
        WriteDisplay(T, 0);  //Write display time date
        WriteDisplay(T, 1);  //Write display time date
        sprintf(T, "%s%s%s",
                isETHConnected() ? "ETH " : "",
                isWIFIConnected() ? "WFI " : "",
                isPPPConnected() ? "PPP " : "");
        WriteDisplay(T, 2);
        sprintf(T, "RAM:%d", esp_get_free_heap_size());
        WriteDisplay(T, 3);
        UpdateDisplay();

#endif

}

static void MainAppServiceSecondary(void *arg)
{
    while (true)
    {
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}

void MainAppServiceStart(void)
{
    ESP_ERROR_CHECK(esp_timer_create(&mainapp_service_timer_args, &mainapp_service_timer));
    ESP_ERROR_CHECK(esp_timer_start_periodic(mainapp_service_timer, 1000000));
    xTaskCreate(MainAppServiceSecondary, "MainAppTask2", 1024 * 3, (void*) 0, 3, NULL);
}

