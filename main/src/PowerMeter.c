/*
 Copyright 2022 Bogdan Pilyugin

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 File name: PowerMeter.c
 Project: EnergyController
 Created on: 2022-07-12
 Author: Bogdan Pilyugin
 
 Description: TODO



 */

#include "PowerMeter.h"
#include "esp_timer.h"
#include "esp_log.h"
#include "driver/gpio.h"

#define CONFIG_POWER_METER_PIN 27
#define CONFIG_VOLTCUR_METER_PIN 33
#define CONFIG_VOLTCUR_SWITCH_PIN 32

#define TAG "PowerMeter"

#define Ki  0.85
#define Ku  0.78
#define Kp  0.732
#define Uref 1.218
#define Uref2 1.484

QueueHandle_t PcnpEventQueue;   // A queue to handle pulse counter events
int pcnt_unit_power = PCNT_UNIT_0;
int pcnt_unit_voltcur = PCNT_UNIT_1;

static int16_t Vrms;
static int16_t Irms;
static int16_t P;

void ReadData(void *arg);

esp_timer_handle_t read_data_timer;
const esp_timer_create_args_t read_data_timer_args = {
        .callback = &ReadData,
        .name = "ReadDataTimer"
};

float GetVrms(void)
{
    return (float) Vrms * (Uref * (1980.0 / 15397)) * Ku;
}

float GetIrms(void)
{
    return (float) Irms * (Uref * (333.3 / 96638)) * Ki;
}

float GetPower(void)
{
    return (float) P * Uref2 * 0.3835 * Kp;
}

void ReadData(void *arg)
{
    static int phazeCounter = 0;
    ++phazeCounter;
    if (phazeCounter == 6)
    {
        pcnt_counter_pause(pcnt_unit_voltcur);
        pcnt_get_counter_value(pcnt_unit_voltcur, &Irms);
        Irms = (int16_t) (Irms-Irms/3);
        gpio_set_level(CONFIG_VOLTCUR_SWITCH_PIN, 1); //start measure volt
        pcnt_counter_clear(pcnt_unit_voltcur);
        pcnt_counter_resume(pcnt_unit_voltcur);
    }
    else if (phazeCounter == 8)
    {
        pcnt_counter_pause(pcnt_unit_power);
        pcnt_counter_pause(pcnt_unit_voltcur);
        pcnt_get_counter_value(pcnt_unit_power, &P);
        P = (int16_t)(P / 2);
        pcnt_get_counter_value(pcnt_unit_voltcur, &Vrms);
        Vrms = (int16_t) (Vrms * 2);
        pcnt_counter_clear(pcnt_unit_power);
        pcnt_counter_clear(pcnt_unit_voltcur);
        pcnt_counter_resume(pcnt_unit_power);
        pcnt_counter_resume(pcnt_unit_voltcur);
        gpio_set_level(CONFIG_VOLTCUR_SWITCH_PIN, 0); //start measure current
        phazeCounter = 0;
    }
}

void PowerMeterMainTask(void *pvParameter)
{
    while (1)
    {
        vTaskDelay(pdMS_TO_TICKS(300));

    }

}

esp_err_t PowerMeterInit(void)
{
    pcnt_config_t pcnt_config = {
            // Set PCNT input signal and control GPIOs
            .pulse_gpio_num = CONFIG_POWER_METER_PIN,
            .ctrl_gpio_num = PCNT_PIN_NOT_USED,
            .channel = PCNT_CHANNEL_0,
            .unit = pcnt_unit_power,
            // What to do on the positive / negative edge of pulse input?
            .pos_mode = PCNT_COUNT_INC,   // Count up on the positive edge
            .neg_mode = PCNT_COUNT_DIS,   // Keep the counter value on the negative edge
            // What to do when control input is low or high?
            // .lctrl_mode = PCNT_MODE_DISABLE, // Reverse counting direction if low
            // .hctrl_mode = PCNT_MODE_DISABLE,    // Keep the primary counter mode if high
            // Set the maximum and minimum limit values to watch
            .counter_h_lim = 32000,
            .counter_l_lim = -32000,
    };
    pcnt_unit_config(&pcnt_config);

    pcnt_config.pulse_gpio_num = CONFIG_VOLTCUR_METER_PIN;
    pcnt_config.unit = pcnt_unit_voltcur;
    pcnt_config.channel = PCNT_CHANNEL_1;

    pcnt_unit_config(&pcnt_config);

    pcnt_set_filter_value(pcnt_unit_power, 100);
    pcnt_set_filter_value(pcnt_unit_voltcur, 100);
    pcnt_filter_enable(pcnt_unit_power);
    pcnt_filter_enable(pcnt_unit_voltcur);

    gpio_set_level(CONFIG_VOLTCUR_SWITCH_PIN, 0); //start measure current
    pcnt_counter_pause(pcnt_unit_power);
    pcnt_counter_pause(pcnt_unit_voltcur);
    pcnt_counter_clear(pcnt_unit_power);
    pcnt_counter_clear(pcnt_unit_voltcur);
    pcnt_counter_resume(pcnt_unit_power);
    pcnt_counter_resume(pcnt_unit_voltcur);

    xTaskCreate(PowerMeterMainTask, "PowerMeterTask", 1024 * 4, (void*) 0, 3, NULL);
    ESP_ERROR_CHECK(esp_timer_create(&read_data_timer_args, &read_data_timer));
    ESP_ERROR_CHECK(esp_timer_start_periodic(read_data_timer, 250000));
    return ESP_OK;
}
