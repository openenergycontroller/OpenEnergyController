/*
 Copyright 2022 Bogdan Pilyugin

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 File name: Relay.c
 Project: EnergyController
 Created on: 2022-07-12
 Author: Bogdan Pilyugin
 
 Description: Driver for hybrid triac-relay scheme

 */

#include "Relay.h"
#include "driver/gpio.h"

static RELAY_STATE rst;
QueueHandle_t RelayCommandsQueueHandle;
TaskHandle_t RelayMainTaskHandle;
StaticQueue_t xStaticRelayCommandsQueue;
uint8_t RelayCommandsQueueStorageArea[RELAY_QUEUE_SIZE * sizeof(relay_command_strust_t)];

void RelayTask(void *pvParameter)
{
    static relay_command_strust_t newcom;
    while(1)
    {
        if (xQueueReceive(RelayCommandsQueueHandle, &newcom, pdMS_TO_TICKS(10)) == pdPASS)
        {
         if (newcom.cmd != RelayGet())
         {
             if (newcom.cmd == RELAY_ON)
             {
                 gpio_set_level(CONFIG_TRIAC_PIN, 1);
                 vTaskDelay(pdMS_TO_TICKS(RELAY_TO_TRIAC_DELAY_MS));
                 gpio_set_level(CONFIG_RELAY_PIN, 1);
                 vTaskDelay(pdMS_TO_TICKS(RELAY_TO_TRIAC_DELAY_MS));
                 gpio_set_level(CONFIG_TRIAC_PIN, 0);
                 gpio_set_level(CONFIG_POWER_LED_PIN, 1);
                 rst = RELAY_ON;
             }
             else if (newcom.cmd == RELAY_OFF)
             {
                 gpio_set_level(CONFIG_TRIAC_PIN, 1);
                 vTaskDelay(pdMS_TO_TICKS(RELAY_TO_TRIAC_DELAY_MS));
                 gpio_set_level(CONFIG_RELAY_PIN, 0);
                 vTaskDelay(pdMS_TO_TICKS(RELAY_TO_TRIAC_DELAY_MS));
                 gpio_set_level(CONFIG_TRIAC_PIN, 0);
                 gpio_set_level(CONFIG_POWER_LED_PIN, 0);
                 rst = RELAY_OFF;
             }

         }
        }
    }

}

esp_err_t RelayInit(void)
{
    RelayCommandsQueueHandle = xQueueCreateStatic(RELAY_QUEUE_SIZE,
                                                  sizeof(relay_command_strust_t),
                                                  RelayCommandsQueueStorageArea,
                                                  &xStaticRelayCommandsQueue);
    rst = RELAY_OFF;
    xTaskCreate(RelayTask, "RelayTask", 1024 * 1, (void*) 0, 3, &RelayMainTaskHandle);
    if (RelayCommandsQueueHandle && RelayMainTaskHandle)
        return ESP_OK;
    else
        return ESP_ERR_NO_MEM;
}


esp_err_t RelaySet(RELAY_STATE state)
{
    relay_command_strust_t command;
    command.cmd = state;
    if (xQueueSend(RelayCommandsQueueHandle, &command, 0) == pdPASS)
        return ESP_OK;
    else
        return ESP_ERR_TIMEOUT;
}

RELAY_STATE RelayGet()
{
    return rst;
}

