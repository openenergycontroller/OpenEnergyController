/*
 Copyright 2022 Bogdan Pilyugin

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 File name: Relay.h
 Project: EnergyController
 Created on: 2022-07-12
 Author: Bogdan Pilyugin
 
 Description: TODO



 */

#ifndef MAIN_INCLUDE_RELAY_H_
#define MAIN_INCLUDE_RELAY_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"

#define CONFIG_RELAY_PIN 25
#define CONFIG_TRIAC_PIN 26
#define CONFIG_POWER_LED_PIN 14

#define RELAY_QUEUE_SIZE 16
#define RELAY_TO_TRIAC_DELAY_MS 50

typedef enum
{
    RELAY_OFF = 0,
    RELAY_ON
} RELAY_STATE;

typedef struct
{
    RELAY_STATE cmd;
} relay_command_strust_t;



esp_err_t RelayInit(void);
esp_err_t RelaySet(RELAY_STATE state);
RELAY_STATE RelayGet();

#endif /* MAIN_INCLUDE_RELAY_H_ */
