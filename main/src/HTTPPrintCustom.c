/*! Copyright 2022 Bogdan Pilyugin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  	 \file HTTPPrintCustom.c
 *    \version 1.0
 * 		 \date 2022-08-18
 *     \author Bogdan Pilyugin
 * 	    \brief    
 *    \details 
 *	\copyright Apache License, Version 2.0
 */
#include "webguiapp.h"
#include "jWrite.h"
#include "AppConfiguration.h"
#include "CronTimers.h"
#include "PowerMeter.h"
#include "Relay.h"
#include "esp_ota_ops.h"

static void HTTPPrint_crontmr(char *VarData, void *arg)
{
   int idx = *((int*)(arg));
   if(idx < CRON_TIMERS_NUMBER)
   {
    char data[256];
    cron_timer_t T;
    memcpy(&T, &GetAppConf()->Timers[idx], sizeof(cron_timer_t));
    jwOpen(data, sizeof(data), JW_OBJECT, JW_COMPACT);
    jwObj_int("num", (unsigned int) T.num);
    jwObj_int("enab", (T.enab)?1:0);
    jwObj_string("name", T.name);
    jwObj_int("obj", (unsigned int) T.obj);
    jwObj_int("act", (unsigned int) T.act);
    jwObj_string("cron", T.cron);
    jwObj_int("del", (T.del)?1:0);
    jwEnd();
    jwClose();
    snprintf(VarData, MAX_DYNVAR_LENGTH, "%s", data);
   }
}

static void HTTPPrint_cronerr(char *VarData, void *arg)
{
    snprintf(VarData, MAX_DYNVAR_LENGTH, GetCronError());
}


static void HTTPPrint_status(char *VarData, void *arg)
{
    if (RelayGet() == RELAY_ON)
        snprintf(VarData, MAX_DYNVAR_LENGTH, "ON");
    else
        snprintf(VarData, MAX_DYNVAR_LENGTH, "OFF");
}

static void HTTPPrint_urms(char *VarData, void *arg)
{
    snprintf(VarData, MAX_DYNVAR_LENGTH, "%4.1f", GetVrms());
}
static void HTTPPrint_irms(char *VarData, void *arg)
{
    snprintf(VarData, MAX_DYNVAR_LENGTH, "%4.3f", GetIrms());
}
static void HTTPPrint_pwr(char *VarData, void *arg)
{
    snprintf(VarData, MAX_DYNVAR_LENGTH, "%4.1f", GetPower());
}
static void HTTPPrint_dname(char *VarData, void *arg)
{
    snprintf(VarData, MAX_DYNVAR_LENGTH, "%s", GetSysConf()->NetBIOSName);
}

static void HTTPPrint_chdname(char *VarData, void *arg)
{
    snprintf(VarData, MAX_DYNVAR_LENGTH, "%s", "ch1");
}
static void HTTPPrint_tact(char *VarData, void *arg)
{
    snprintf(VarData, MAX_DYNVAR_LENGTH, "1");
}
static void HTTPPrint_fver(char *VarData, void *arg)
{
    esp_app_desc_t cur_app_info;
    if (esp_ota_get_partition_description(esp_ota_get_running_partition(), &cur_app_info) == ESP_OK)
    {
        snprintf(VarData, MAX_DYNVAR_LENGTH, "%s", cur_app_info.version);
    }
}

//Default string if not found handler
static void HTTPPrint_DEF(char *VarData, void *arg)
{
    snprintf(VarData, MAX_DYNVAR_LENGTH, "#DEF");
}

static void HTTPPrint_status_fail(char *VarData, void *arg)
{
    snprintf(VarData, MAX_DYNVAR_LENGTH, "none");
}

dyn_var_handler_t HANDLERS_ARRAY_CUST[] = {

        { "status", sizeof("status") - 1, &HTTPPrint_status },
        { "urms", sizeof("urms") - 1, &HTTPPrint_urms },
        { "irms", sizeof("irms") - 1, &HTTPPrint_irms },
        { "pwr", sizeof("pwr") - 1, &HTTPPrint_pwr },
        { "dname", sizeof("dname") - 1, &HTTPPrint_dname },
        { "chname", sizeof("chname") - 1, &HTTPPrint_chdname },
        { "tact", sizeof("tact") - 1, &HTTPPrint_tact },
        { "fver", sizeof("fver") - 1, &HTTPPrint_fver },

        { "crontmr", sizeof("crontmr") - 1, &HTTPPrint_crontmr },
        { "cronerr", sizeof("cronerr") - 1, &HTTPPrint_cronerr },
        /*ERROR report*/
        { "status_fail", sizeof("status_fail") - 1, &HTTPPrint_status_fail },

};


int HTTPPrintCustom(httpd_req_t *req, char *buf, char *var, int arg)
{
    char VarData[MAX_DYNVAR_LENGTH];
    bool fnd = false;
    //searching for tag in handles array
    for (int i = 0; i < (sizeof(HANDLERS_ARRAY_CUST) / sizeof(HANDLERS_ARRAY_CUST[0])); ++i)
    {
        if (strcmp(var, HANDLERS_ARRAY_CUST[i].tag) == 0
                && HANDLERS_ARRAY_CUST[i].HandlerRoutine != NULL)
        {
            HANDLERS_ARRAY_CUST[i].HandlerRoutine(VarData, (void*) &arg);
            fnd = true;
            break;
        }
    }
    if (!fnd)
    {
            HTTPPrint_DEF(VarData, NULL);
    }
    int dLen = strlen(VarData);
    memcpy(buf, VarData, dLen);
    return dLen;
}
