 /* 
  Copyright 2022 Bogdan Pilyugin
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 
    File name: PowerMeter.h
      Project: EnergyController
   Created on: 2022-07-12
       Author: Bogdan Pilyugin
   
  Description: TODO   


  
 */	

#ifndef MAIN_INCLUDE_POWERMETER_H_
#define MAIN_INCLUDE_POWERMETER_H_

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "driver/pcnt.h"

esp_err_t PowerMeterInit(void);
float GetVrms(void);
float GetIrms(void);
float GetPower(void);


#endif /* MAIN_INCLUDE_POWERMETER_H_ */
